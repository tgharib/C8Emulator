This project is a [CHIP-8](http://en.wikipedia.org/wiki/CHIP-8) emulator.

Arguments
---------
    USAGE:

       ./C8Emulator  [-u] [-s <integer>] -p <string> [--] [--version] [-h]


    Where:

       -u,  --use-interpreter
         Use the interpreter instead of the dynamic recompiler for higher
         accuracy (slower speed)

       -s <integer>,  --scale <integer>
         Specify the graphics scaling (default is 20)

       -p <string>,  --path <string>
         (required)  Path to the rom

       --,  --ignore_rest
         Ignores the rest of the labeled arguments following this flag.

       --version
         Displays version information and exits.

       -h,  --help
         Displays usage information and exits.


       Chip-8 Emulator

Libraries required
------------------

1. SFML
2. FSMA for 2D/3D arrays (included in git repo & autoconfigured for use with CMake)
3. [TCLAP](http://tclap.sourceforge.net/) for command-line argument parsing (included in git repo & autoconfigured for use with CMake)
4. [Catch](https://github.com/philsquared/Catch) for unit testing (included in git repo & autoconfigured for use with CMake)

Build Instructions For Linux & OS X
-----------------------------------

1. Install the SFML library: `sudo apt-get install libsfml-dev` on Ubuntu or `sudo pacman -S sfml` on ArchLinux
2. `git clone --recursive https://github.com/tgharib/C8Emulator.git`
3. `cd C8Emulator && mkdir build && cd build`
4. `cmake ..`
5. `make`

Build Instructions For Windows
-------------------------------

1. `git clone --recursive https://github.com/tgharib/C8Emulator.git`
2. `cd C8Emulator && mkdir build && cd build`
3. Install the [SFML library](http://www.sfml-dev.org/download.php) in C8Emulator\libs\. C8Emulator\libs\SFML\bin should contain all the SFML DLLs.
4. `cmake ..`
5. Open up the Visual Studio solution file and compile.

Mapping CHIP-8 Keypad to PC Keyboard
------------------------------------

    Keypad                   Keyboard
    +-+-+-+-+                +-+-+-+-+
    |1|2|3|C|                |1|2|3|4|
    +-+-+-+-+                +-+-+-+-+
    |4|5|6|D|                |Q|W|E|R|
    +-+-+-+-+       =>       +-+-+-+-+
    |7|8|9|E|                |A|S|D|F|
    +-+-+-+-+                +-+-+-+-+
    |A|0|B|F|                |Z|X|C|V|
    +-+-+-+-+                +-+-+-+-+

TODO
----
* find out why the emulator receives a sf::Event::Closed right at the start
* figure out a way to test 0xFX0A
* make sure everything is only public if required

asmjit vs xbyak
----------------

AsmJit Pros:

1. Includes x86compiler (not used since it does not support global variables)
2. ARM support (x86assembler requires explicitly specifying x86 registers so a port to ARM would be needed)

AsmJit Cons:

1. Weird syntax and poor documentation
2. Doesn't seem to support generating machine code at arbitrary memory locations
