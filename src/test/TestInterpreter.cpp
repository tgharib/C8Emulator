#include <catch.hpp>
#include <fstream>
#include <iostream>
#include "../production/EmulatorLoop.h"

TEST_CASE("Instructions are tested.") {
    std::ofstream outfile("dummy_rom");
    outfile.close();

    EmulatorLoop emu("dummy_rom", 1, false);

    SECTION("0x00E0: Clears the screen") {
        for (int col = 0; col < 64; col++) {
            for (int row = 0; row < 32; row++) {
                CPU::gfx.at(col, row) = 1;
            }
        }

        emu.interpreter.emulateOpcode(0x00E0);

        for (int col = 0; col < 64; col++) {
            for (int row = 0; row < 32; row++) {
                if (CPU::gfx.at(col, row) == 1) {
                    REQUIRE(false);
                }
            }
        }
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x00EE: Returns from subroutine") {
        CPU::pc = 0x21F;                        // pc starts at 0x21F
        emu.interpreter.emulateOpcode(0x2123);  // call subroutine at 123
        emu.interpreter.emulateOpcode(0x00EE);  // return from subroutine
        REQUIRE(CPU::pc == 0x221);
    }

    SECTION("0x1NNN: Jumps to address NNN.") {
        emu.interpreter.emulateOpcode(0x1321);
        REQUIRE(CPU::pc == 0x321);
    }

    SECTION("0x2NNN: Calls subroutine at NNN.") {
        CPU::pc = 0x212;                        // pc starts at 0x212
        emu.interpreter.emulateOpcode(0x2332);  // call subroutine at 332
        REQUIRE(CPU::pc == 0x332);
        REQUIRE(CPU::stack.at(CPU::sp) == 0x214);
    }

    SECTION("0x3XNN: Skips the next instruction if V.at(X) equals NN.") {
        CPU::registers.at(0x2) = 0x34;
        emu.interpreter.emulateOpcode(0x3223);
        REQUIRE(CPU::pc == 0x202);
        emu.interpreter.emulateOpcode(0x3234);
        REQUIRE(CPU::pc == 0x206);
    }

    SECTION("0x4XNN: Skips the next instruction if V.at(X) doesn't equal NN.") {
        CPU::registers.at(0x2) = 0x34;
        emu.interpreter.emulateOpcode(0x4223);
        REQUIRE(CPU::pc == 0x204);
        emu.interpreter.emulateOpcode(0x4234);
        REQUIRE(CPU::pc == 0x206);
    }

    SECTION("0x5XY0: Skips the next instruction if V.at(X) equals V.at(Y).") {
        CPU::registers.at(0x2) = 0x34;

        CPU::registers.at(0x3) = 0x34;
        emu.interpreter.emulateOpcode(0x5230);
        REQUIRE(CPU::pc == 0x204);

        CPU::registers.at(0x3) = 0x35;
        emu.interpreter.emulateOpcode(0x5230);
        REQUIRE(CPU::pc == 0x206);
    }

    SECTION("0x6XNN: Sets V.at(X) to NN.") {
        emu.interpreter.emulateOpcode(0x6030);
        REQUIRE(CPU::registers.at(0x0) == 0x30);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x7XNN: Adds NN to V.at(X).") {
        CPU::registers.at(0x1) = 0x2;
        emu.interpreter.emulateOpcode(0x7130);
        REQUIRE(CPU::registers.at(0x1) == 0x32);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x8XY0: Sets V.at(X) to the value of V.at(Y).") {
        CPU::registers.at(0xA) = 0x10;
        emu.interpreter.emulateOpcode(0x8BA0);
        REQUIRE(CPU::registers.at(0XB) == 0x10);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x8XY1: Sets V.at(X) to (V.at(X) | V.at(Y)).") {
        CPU::registers.at(0xA) = 0xA;
        CPU::registers.at(0xB) = 0xB;
        emu.interpreter.emulateOpcode(0x8AB1);
        REQUIRE(CPU::registers.at(0xA) == 0xB);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x8XY2: Sets V.at(X) to (V.at(X) & V.at(Y)).") {
        CPU::registers.at(0xA) = 0xB;
        CPU::registers.at(0xB) = 0xA;
        emu.interpreter.emulateOpcode(0x8AB2);
        REQUIRE(CPU::registers.at(0xA) == 0xA);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x8XY3: Sets V.at(X) to (V.at(X) ^ V.at(Y)).") {
        CPU::registers.at(0xA) = 0xA;
        CPU::registers.at(0xB) = 0xB;
        emu.interpreter.emulateOpcode(0x8AB3);
        REQUIRE(CPU::registers.at(0xA) == 0x1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY4 without carry: Adds V.at(Y) to V.at(X). V.at(F) is set to 1 when "
        "there's a carry, and to 0 when there isn't.") {
        CPU::registers.at(0xA) = 0xA;
        CPU::registers.at(0xB) = 0xB;
        emu.interpreter.emulateOpcode(0x8AB4);
        REQUIRE(CPU::registers.at(0xA) == 0x15);
        REQUIRE(CPU::registers.at(0xF) == 0x0);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY4 with carry: Adds V.at(Y) to V.at(X). V.at(F) is set to 1 when "
        "there's a carry, and to 0 when there isn't.") {
        CPU::registers.at(0xA) = 0xFF;
        CPU::registers.at(0xB) = 0xFF;
        emu.interpreter.emulateOpcode(0x8AB4);
        REQUIRE(CPU::registers.at(0xA) == 0xFE);
        REQUIRE(CPU::registers.at(0xF) == 0x1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY5 without borrow: V.at(Y) is subtracted from V.at(X). V.at(F) is "
        "set to 0 when there's a borrow, and 1 when there isn't.") {
        CPU::registers.at(0xA) = 0xF;
        CPU::registers.at(0xB) = 0xB;
        emu.interpreter.emulateOpcode(0x8AB5);
        REQUIRE(CPU::registers.at(0xA) == 0x4);
        REQUIRE(CPU::registers.at(0xF) == 0x1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY5 with borrow: V.at(Y) is subtracted from V.at(X). V.at(F) is set "
        "to 0 when there's a borrow, and 1 when there isn't.") {
        CPU::registers.at(0xA) = 0xA;
        CPU::registers.at(0xB) = 0xB;
        emu.interpreter.emulateOpcode(0x8AB5);
        REQUIRE(CPU::registers.at(0xA) == 0xFF);
        REQUIRE(CPU::registers.at(0xF) == 0x0);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY6: Shifts V.at(X) right by one. V.at(F) is set to the value of the "
        "least significant bit of V.at(X) before the shift.") {
        CPU::registers.at(0xA) = 0xB;
        emu.interpreter.emulateOpcode(0x8A06);
        REQUIRE(CPU::registers.at(0xA) == 0x5);
        REQUIRE(CPU::registers.at(0xF) == 0x1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY7 without borrow: Sets V.at(X) to V.at(Y) minus V.at(X). V.at(F) "
        "is set to 0 when there's a borrow, and 1 when there isn't.") {
        CPU::registers.at(0xA) = 0xA;
        CPU::registers.at(0xB) = 0xB;
        emu.interpreter.emulateOpcode(0x8AB7);
        REQUIRE(CPU::registers.at(0xA) == 0x1);
        REQUIRE(CPU::registers.at(0xF) == 0x1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XY7 with borrow: Sets V.at(X) to V.at(Y) minus V.at(X). V.at(F) is "
        "set to 0 when there's a borrow, and 1 when there isn't.") {
        CPU::registers.at(0xA) = 0xB;
        CPU::registers.at(0xB) = 0xA;
        emu.interpreter.emulateOpcode(0x8AB7);
        REQUIRE(CPU::registers.at(0xA) == 0xFF);
        REQUIRE(CPU::registers.at(0xF) == 0x0);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0x8XYE: Shifts V.at(X) left by one. V.at(F) is set to the value of the "
        "most significant bit of V.at(X) before the shift.") {
        CPU::registers.at(0xA) = 0x8A;
        emu.interpreter.emulateOpcode(0x8ABE);
        REQUIRE(CPU::registers.at(0xA) == 0x14);
        REQUIRE(CPU::registers.at(0xF) == 0x1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0x9XY0: Skips the next instruction if V.at(X) doesn't equal V.at(Y).") {
        CPU::registers.at(0xA) = 0x34;

        CPU::registers.at(0xB) = 0x34;
        emu.interpreter.emulateOpcode(0x9AB0);
        REQUIRE(CPU::pc == 0x202);

        CPU::registers.at(0xB) = 0x35;
        emu.interpreter.emulateOpcode(0x9AB0);
        REQUIRE(CPU::pc == 0x206);
    }

    SECTION("0xANNN: Sets I to the address NNN") {
        emu.interpreter.emulateOpcode(0xA123);
        REQUIRE(CPU::I == 0x123);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xBNNN: Jumps to the address NNN plus V.at(0).") {
        CPU::registers.at(0) = 0x1;
        emu.interpreter.emulateOpcode(0xB123);
        REQUIRE(CPU::pc == 0x124);
    }

    SECTION(
        "0xDXYN: Draws a sprite at coordinate (V.at(X), V.at(Y)) that has a "
        "width of 8 pixels and a height of N pixels. "
        "Each row of 8 pixels is read as bit-coded starting from memory location "
        "I; I value doesn’t change after "
        "the execution of this instruction. As described above, V.at(F) is set "
        "to 1 if any screen pixels are flipped "
        "from set to unset when the sprite is drawn, and to 0 if that doesn’t happen.") {
        CPU::registers.at(0xA) = 0xF;
        CPU::registers.at(0xB) = 0xF;
        CPU::I = 10;  // memory.at(10) is the number 2

        emu.interpreter.emulateOpcode(0xDAB5);

        REQUIRE(CPU::gfx.at(0xF, 0xF) == 1);
        REQUIRE(CPU::gfx.at(0xF + 1, 0xF) == 1);
        REQUIRE(CPU::gfx.at(0xF + 2, 0xF) == 1);
        REQUIRE(CPU::gfx.at(0xF + 3, 0xF) == 1);
        REQUIRE(CPU::gfx.at(0xF + 4, 0xF) == 0);
        REQUIRE(CPU::gfx.at(0xF + 5, 0xF) == 0);
        REQUIRE(CPU::gfx.at(0xF + 6, 0xF) == 0);
        REQUIRE(CPU::gfx.at(0xF + 7, 0xF) == 0);

        REQUIRE(CPU::gfx.at(0xF, 0xF + 1) == 0);
        REQUIRE(CPU::gfx.at(0xF + 1, 0xF + 1) == 0);
        REQUIRE(CPU::gfx.at(0xF + 2, 0xF + 1) == 0);
        REQUIRE(CPU::gfx.at(0xF + 3, 0xF + 1) == 1);
        REQUIRE(CPU::gfx.at(0xF + 4, 0xF + 1) == 0);
        REQUIRE(CPU::gfx.at(0xF + 5, 0xF + 1) == 0);
        REQUIRE(CPU::gfx.at(0xF + 6, 0xF + 1) == 0);
        REQUIRE(CPU::gfx.at(0xF + 7, 0xF + 1) == 0);

        REQUIRE(CPU::gfx.at(0xF, 0xF + 2) == 1);
        REQUIRE(CPU::gfx.at(0xF + 1, 0xF + 2) == 1);
        REQUIRE(CPU::gfx.at(0xF + 2, 0xF + 2) == 1);
        REQUIRE(CPU::gfx.at(0xF + 3, 0xF + 2) == 1);
        REQUIRE(CPU::gfx.at(0xF + 4, 0xF + 2) == 0);
        REQUIRE(CPU::gfx.at(0xF + 5, 0xF + 2) == 0);
        REQUIRE(CPU::gfx.at(0xF + 6, 0xF + 2) == 0);
        REQUIRE(CPU::gfx.at(0xF + 7, 0xF + 2) == 0);

        REQUIRE(CPU::gfx.at(0xF, 0xF + 3) == 1);
        REQUIRE(CPU::gfx.at(0xF + 1, 0xF + 3) == 0);
        REQUIRE(CPU::gfx.at(0xF + 2, 0xF + 3) == 0);
        REQUIRE(CPU::gfx.at(0xF + 3, 0xF + 3) == 0);
        REQUIRE(CPU::gfx.at(0xF + 4, 0xF + 3) == 0);
        REQUIRE(CPU::gfx.at(0xF + 5, 0xF + 3) == 0);
        REQUIRE(CPU::gfx.at(0xF + 6, 0xF + 3) == 0);
        REQUIRE(CPU::gfx.at(0xF + 7, 0xF + 3) == 0);

        REQUIRE(CPU::gfx.at(0xF, 0xF + 4) == 1);
        REQUIRE(CPU::gfx.at(0xF + 1, 0xF + 4) == 1);
        REQUIRE(CPU::gfx.at(0xF + 2, 0xF + 4) == 1);
        REQUIRE(CPU::gfx.at(0xF + 3, 0xF + 4) == 1);
        REQUIRE(CPU::gfx.at(0xF + 4, 0xF + 4) == 0);
        REQUIRE(CPU::gfx.at(0xF + 5, 0xF + 4) == 0);
        REQUIRE(CPU::gfx.at(0xF + 6, 0xF + 4) == 0);
        REQUIRE(CPU::gfx.at(0xF + 7, 0xF + 4) == 0);

        REQUIRE(CPU::registers.at(0xF) == 0x0);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xEX9E: Skips the next instruction if the key stored in V.at(X) is pressed") {
        emu.interpreter.emulateOpcode(0xEA9E);
        REQUIRE(CPU::pc == 0x202);
        CPU::registers.at(0xA) = 0xB;
        CPU::keys.at(0xB) = 1;
        emu.interpreter.emulateOpcode(0xEA9E);
        REQUIRE(CPU::pc == 0x206);
    }

    SECTION(
        "0xEXA1: Skips the next instruction if the key stored in V.at(X) isn't "
        "pressed") {
        emu.interpreter.emulateOpcode(0xEAA1);
        REQUIRE(CPU::pc == 0x204);
        CPU::registers.at(0xA) = 0xB;
        CPU::keys.at(0xB) = 1;
        emu.interpreter.emulateOpcode(0xEAA1);
        REQUIRE(CPU::pc == 0x206);
    }

    SECTION("0xFX07: Sets V.at(X) to the value of the delay timer.") {
        CPU::delay_timer = 30;
        emu.interpreter.emulateOpcode(0xFC07);
        REQUIRE(CPU::registers.at(0xC) == 30);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xFX0A: A key press is awaited. Then, the hexadecimal associated to the key is stored in V.at(X).") {
        CPU::keys.at(0xC) = 1;
        emu.interpreter.emulateOpcode(0xF20A);
        REQUIRE(CPU::registers.at(0x2) == 0xC);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xFX15: Sets the delay timer to V.at(X).") {
        CPU::registers.at(0xC) = 30;
        emu.interpreter.emulateOpcode(0xFC15);
        REQUIRE(CPU::delay_timer == 30);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xFX18: Sets the sound timer to V.at(X).") {
        CPU::registers.at(0xC) = 30;
        emu.interpreter.emulateOpcode(0xFC18);
        REQUIRE(CPU::sound_timer == 30);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0xFX1E without carry: Adds V.at(X) to I. V.at(F) is set to 1 when there "
        "is a carry and 0 when there isn't.") {
        CPU::registers.at(0xC) = 0xA;
        CPU::I = 0xB;
        emu.interpreter.emulateOpcode(0xFC1E);
        REQUIRE(CPU::I == 0x15);
        REQUIRE(CPU::registers.at(0xF) == 0);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0xFX1E with carry: Adds V.at(X) to I. V.at(F) is set to 1 when there is "
        "a carry and 0 when there isn't.") {
        CPU::registers.at(0xC) = 0x1;
        CPU::I = 0xFFF;
        emu.interpreter.emulateOpcode(0xFC1E);
        REQUIRE(CPU::I == 0x1000);
        REQUIRE(CPU::registers.at(0xF) == 1);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0xFX29: Sets I to the memory location of the sprite associated to the "
        "hexadecimal in V.at(X). Characters 0-F "
        "(in hexadecimal) are represented by a 4x5 font.") {
        CPU::registers.at(0xA) = 0xF;

        emu.interpreter.emulateOpcode(0xFA29);

        REQUIRE(CPU::I == 75);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION(
        "0xFX33: Stores the binary-coded decimal representation of V.at(X), with "
        "the most significant of three digits "
        "at the address in I, the middle digit at I plus 1, and the least "
        "significant digit at I plus 2. (In other "
        "words, take the decimal representation of V.at(X), place the hundreds "
        "digit in memory at location in I, the "
        "tens digit at location I+1, and the ones digit at location I+2.)") {
        CPU::registers.at(0xA) = 23;
        CPU::I = 0x100;

        emu.interpreter.emulateOpcode(0xFA33);

        REQUIRE(CPU::memory.at(CPU::I) == 0);
        REQUIRE(CPU::memory.at(CPU::I + 1) == 2);
        REQUIRE(CPU::memory.at(CPU::I + 2) == 3);
        REQUIRE(CPU::I == 0x100);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xFX55: Stores V.at(0) to V.at(X) in memory starting at address I. I is unchanged.") {
        CPU::I = 0x300;
        CPU::registers.at(0x0) = 0x2;
        CPU::registers.at(0x1) = 0xA;
        CPU::registers.at(0x2) = 0xB;
        CPU::registers.at(0x3) = 0xD;
        CPU::registers.at(0x4) = 0x3;
        CPU::registers.at(0x5) = 0x5;
        CPU::registers.at(0x6) = 0x3;
        CPU::registers.at(0x7) = 0x9;
        CPU::registers.at(0x8) = 0xE;
        CPU::registers.at(0x9) = 0x0;
        CPU::registers.at(0xA) = 0x3;
        CPU::registers.at(0xB) = 0x2;
        CPU::registers.at(0xC) = 0x1;
        CPU::registers.at(0xD) = 0x7;
        CPU::registers.at(0xE) = 0xA;
        CPU::registers.at(0xF) = 0xF;

        emu.interpreter.emulateOpcode(0xFF55);

        for (uint8_t i = 0; i < 16; i++) {
            REQUIRE(CPU::memory.at(CPU::I + i) == CPU::registers.at(i));
        }

        REQUIRE(CPU::I == 0x300);
        REQUIRE(CPU::pc == 0x202);
    }

    SECTION("0xFX65: Fills V.at(0) to V.at(X) with values from memory starting at address I. I is unchanged.") {
        CPU::I = 0x300;
        CPU::memory.at(CPU::I + 0x0) = 0x2;
        CPU::memory.at(CPU::I + 0x1) = 0xA;
        CPU::memory.at(CPU::I + 0x2) = 0xB;
        CPU::memory.at(CPU::I + 0x3) = 0xD;
        CPU::memory.at(CPU::I + 0x4) = 0x3;
        CPU::memory.at(CPU::I + 0x5) = 0x5;
        CPU::memory.at(CPU::I + 0x6) = 0x3;
        CPU::memory.at(CPU::I + 0x7) = 0x9;
        CPU::memory.at(CPU::I + 0x8) = 0xE;
        CPU::memory.at(CPU::I + 0x9) = 0x0;
        CPU::memory.at(CPU::I + 0xA) = 0x3;
        CPU::memory.at(CPU::I + 0xB) = 0x2;
        CPU::memory.at(CPU::I + 0xC) = 0x1;
        CPU::memory.at(CPU::I + 0xD) = 0x7;
        CPU::memory.at(CPU::I + 0xE) = 0xA;
        CPU::memory.at(CPU::I + 0xF) = 0xF;

        emu.interpreter.emulateOpcode(0xFF65);

        for (uint8_t i = 0; i < 16; i++) {
            REQUIRE(CPU::memory.at(CPU::I + i) == CPU::registers.at(i));
        }

        REQUIRE(CPU::I == 0x300);
        REQUIRE(CPU::pc == 0x202);
    }
}
