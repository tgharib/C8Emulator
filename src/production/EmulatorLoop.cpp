#include "EmulatorLoop.h"
#include <fstream>
#include <iostream>

EmulatorLoop::EmulatorLoop(const std::string pathToRom, int gfxScaling, bool useInterpreter)
    : sfmlWindow(gfxScaling), useInterpreter(useInterpreter), interpreter(sfmlWindow) {
    if (beepBuffer.loadFromFile("beep.ogg")) {
        beepSound.setBuffer(beepBuffer);
    } else {
        std::cout << "Unable to load beep sound" << std::endl;
    }

    loadROM(pathToRom);
}

void EmulatorLoop::loadROM(std::string path) {
    // Open ROM
    std::streampos fileSize;
    std::ifstream file(path, std::ios::binary);

    if (!file.is_open()) {
        std::cout << "Unable to load ROM because the file is already open." << std::endl;
        exit(1);
    }

    // Get ROM size
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    if (fileSize > 4096 - 0x200) {  // maxmium ROM size = 4096 bytes of memory - 0x200 (0x200 is the ROM starting point)
        std::cout << "Unable to load ROM because the ROM is too large to load into the Chip-8's memory. ROM size: "
                  << fileSize << " bytes" << std::endl;
        exit(1);
    }

    // Load ROM into memory.at(0x200)
    file.read((char*)&CPU::memory.at(0x200), fileSize);
}

int EmulatorLoop::mainLoop() {
    int instructionsCount = 0;

    while (true) {
        sf::Clock clock;

        sfmlWindow.takeUserInput();

        if (useInterpreter) {
            interpreter.emulateNextOpcode();
        } else {
            interpreter.emulateNextOpcode();
        }

        if (CPU::drawFlag) {
            sfmlWindow.drawGraphics();
        }

        instructionsCount++;
        if (instructionsCount == 9) {
            instructionsCount = 0;
            decreaseTimers();  // timers are decreased 60 times per second
        }

        sf::Int64 dt = clock.getElapsedTime().asMicroseconds();

        while (dt < 1852) {  // execute 540 opcodes per second
            dt = clock.getElapsedTime().asMicroseconds();
        }

        sfmlWindow.window.setTitle(sfmlWindow.windowTitle + " Instructions Per Second: " +
                                   std::to_string(1000000 / dt));
    }
    return 0;
}

void EmulatorLoop::decreaseTimers() {
    if (CPU::delay_timer > 0) {
        CPU::delay_timer--;
    }

    if (CPU::sound_timer > 0) {
        beepSound.play();
        CPU::sound_timer--;
    }
}
