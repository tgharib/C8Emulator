#include <tclap/CmdLine.h>
#include <iostream>
#include "EmulatorLoop.h"
#include "dynamic_recompiler/CachesManager.h"

int main(int argc, char** argv) {
    std::string pathToRom("");
    int gfxScaling = 20;
    bool useInterpreter = false;

    try {
        TCLAP::CmdLine cmd("Chip-8 Emulator", ' ', "1.0");
        TCLAP::ValueArg<std::string> pathArg("p", "path", "Path to the rom", true, "", "string", cmd);
        TCLAP::ValueArg<int> scaleArg("s", "scale", "Specify the graphics scaling (default is 20)", false, 20,
                                      "integer", cmd);
        TCLAP::SwitchArg interpreterSwitch(
            "u", "use-interpreter",
            "Use the interpreter instead of the dynamic recompiler for higher accuracy (slower speed)", cmd, false);

        cmd.parse(argc, argv);

        pathToRom = pathArg.getValue();
        gfxScaling = scaleArg.getValue();
        useInterpreter = interpreterSwitch.getValue();
    } catch (TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    CachesManager test;
    EmulatorLoop emuInstance(pathToRom, gfxScaling, useInterpreter);
    return emuInstance.mainLoop();
}
