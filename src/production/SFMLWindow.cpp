#include "SFMLWindow.h"
#include "EmulatorLoop.h"

SFMLWindow::SFMLWindow(int gfxScaling)
    : scale(gfxScaling),
      videoMode(static_cast<unsigned int>(64 * scale), static_cast<unsigned int>(32 * scale)),
      window(videoMode, windowTitle) {}

void SFMLWindow::drawGraphics() {
    sf::Image image;
    image.create(64, 32, sf::Color::Black);

    for (int col = 0; col < 64; col++) {
        for (int row = 0; row < 32; row++) {
            sf::Color color;

            if (CPU::gfx.at(col, row) == 0) {
                color = sf::Color::Black;
            } else {
                color = sf::Color::White;
            }

            image.setPixel(col, row, color);
        }
    }

    sf::Texture texture;
    texture.loadFromImage(image);
    sf::Sprite sprite(texture);
    sprite.setScale((float)scale, (float)scale);

    window.clear();
    window.draw(sprite);
    window.display();
}

/* This method is called from the emulator loop and from the 0xFX0A instruction
 *  Returns true if it was a KeyPressed event. Otherwise, returns false.
 */
bool SFMLWindow::takeUserInput() {
    sf::Event event;
    window.pollEvent(event);

    switch (event.type) {
        case sf::Event::Closed: {
            // exit(0);
        }
            return false;

        case sf::Event::KeyPressed: {
            setKey(event.key.code, 1);
        }
            return true;

        case sf::Event::KeyReleased: {
            setKey(event.key.code, 0);
        }
            return false;

        default:
            return false;
    }
}

void SFMLWindow::setKey(sf::Keyboard::Key key, int valueToSet) {
    switch (key) {
        case sf::Keyboard::Escape: {
            exit(0);
        } break;

        case sf::Keyboard::Num1: {  // 1
            CPU::keys.at(0x1) = valueToSet;
            lastKeyChanged = 0x1;
        } break;

        case sf::Keyboard::Num2: {  // 2
            CPU::keys.at(0x2) = valueToSet;
            lastKeyChanged = 0x2;
        } break;

        case sf::Keyboard::Num3: {  // 3
            CPU::keys.at(0x3) = valueToSet;
            lastKeyChanged = 0x3;
        } break;

        case sf::Keyboard::Num4: {  // C
            CPU::keys.at(0xC) = valueToSet;
            lastKeyChanged = 0xC;
        } break;

        case sf::Keyboard::Q: {  // 4
            CPU::keys.at(0x4) = valueToSet;
            lastKeyChanged = 0x4;
        } break;

        case sf::Keyboard::W: {  // 5
            CPU::keys.at(0x5) = valueToSet;
            lastKeyChanged = 0x5;
        } break;

        case sf::Keyboard::E: {  // 6
            CPU::keys.at(0x6) = valueToSet;
            lastKeyChanged = 0x6;
        } break;

        case sf::Keyboard::R: {  // D
            CPU::keys.at(0xD) = valueToSet;
            lastKeyChanged = 0xD;
        } break;

        case sf::Keyboard::A: {  // 7
            CPU::keys.at(0x7) = valueToSet;
            lastKeyChanged = 0x7;
        } break;

        case sf::Keyboard::S: {  // 8
            CPU::keys.at(0x8) = valueToSet;
            lastKeyChanged = 0x8;
        } break;

        case sf::Keyboard::D: {  // 9
            CPU::keys.at(0x9) = valueToSet;
            lastKeyChanged = 0x9;
        } break;

        case sf::Keyboard::F: {  // E
            CPU::keys.at(0xE) = valueToSet;
            lastKeyChanged = 0xE;
        } break;

        case sf::Keyboard::Z: {  // A
            CPU::keys.at(0xA) = valueToSet;
            lastKeyChanged = 0xA;
        } break;

        case sf::Keyboard::X: {  // 0
            CPU::keys.at(0x0) = valueToSet;
            lastKeyChanged = 0x0;
        } break;

        case sf::Keyboard::C: {  // B
            CPU::keys.at(0xB) = valueToSet;
            lastKeyChanged = 0xB;
        } break;

        case sf::Keyboard::V: {  // F
            CPU::keys.at(0xF) = valueToSet;
            lastKeyChanged = 0xF;
        } break;

        default:
            break;
    }
}
