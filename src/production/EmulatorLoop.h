#pragma once
#include "CPU.h"
#include "SFMLWindow.h"
#include "dynamic_recompiler/Translator.h"
#include "interpreter/Interpreter.h"

class EmulatorLoop {
   private:
    SFMLWindow sfmlWindow;
    bool useInterpreter;
    sf::SoundBuffer beepBuffer{};
    sf::Sound beepSound{};

    void loadROM(std::string path);
    void decreaseTimers();

   public:
    EmulatorLoop(const std::string pathToRom, int gfxScaling, bool useInterpreter);
    Interpreter interpreter;

    int mainLoop();
};
