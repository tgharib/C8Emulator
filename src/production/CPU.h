#pragma once
#include <SFML/Audio.hpp>
#include <array>
#include <array_2d.hpp>
#include <random>

class CPU {
   public:
    static bool drawFlag;
    static fsma::array_2d<uint8_t, 64, 32> gfx;  // each byte will be 1 or 0 (width is 64; height is 32)
    static std::array<uint8_t, 16> keys;         // HEX-based keypad with keys from 0 to 15
                                                 // each byte is either 1 (pressed) or 0 (unpressed)
    static std::array<uint8_t, 4096> memory;     // 4096 bytes of memory
    // each opcode is 2 bytes (four 4-bit hexadecimal characters)
    // ROM starts at byte 512 (0x200) in memory. First 80 bytes in memory are the fontset.

    static std::array<uint8_t, 16> registers;  // 16 8-bit registers
    static uint16_t I;                         // index register (value can be from 0x000 to 0xFFF)
    static uint16_t pc;                        // program counter; specifies memory location to execute next instruction
    static uint8_t delay_timer;                // timer register that when set above 0, will count to 0 at 60 hz
    static uint8_t sound_timer;                // timer register that when set above 0, will count to 0 at 60 hz
    static std::array<uint16_t, 16> stack;     // stack is 16 16-bit values to store addresses before you jump
    static uint8_t sp;                         // indicates current position in stack
    static const std::array<uint8_t, 80> fontset;
};
