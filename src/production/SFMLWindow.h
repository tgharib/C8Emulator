#pragma once
#include <SFML/Graphics.hpp>
#include "CPU.h"

class SFMLWindow {
   public:
    SFMLWindow(int gfxScaling);

    int scale = 0;
    const std::string windowTitle = "Chip-8 Emulator";
    uint8_t lastKeyChanged = 0;
    sf::VideoMode videoMode;
    sf::RenderWindow window;

    void drawGraphics();
    bool takeUserInput();
    void setKey(sf::Keyboard::Key key, int value);
};
