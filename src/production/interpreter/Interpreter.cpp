#include "Interpreter.h"
#include <iostream>
#include <thread>

Interpreter::Interpreter(SFMLWindow& sfmlWindow) : sfmlWindow(sfmlWindow) {}

void Interpreter::emulateNextOpcode() {
    emulateOpcode(CPU::memory.at(CPU::pc) << 8 | CPU::memory.at(CPU::pc + 1));
    // since an opcode is two bytes, an opcode is the concatenation of the next
    // memory address and the memory address after that
}

void Interpreter::emulateOpcode(uint16_t opcode) {
    switch (opcode & 0xF000) {
        case 0x0000: {
            handleOpcode0(opcode);
        } break;

        case 0x1000: {
            handleOpcode1(opcode);
        } break;

        case 0x2000: {
            handleOpcode2(opcode);
        } break;

        case 0x3000: {
            handleOpcode3(opcode);
        } break;

        case 0x4000: {
            handleOpcode4(opcode);
        } break;

        case 0x5000: {
            handleOpcode5(opcode);
        } break;

        case 0x6000: {
            handleOpcode6(opcode);
        } break;

        case 0x7000: {
            handleOpcode7(opcode);
        } break;

        case 0x8000: {
            handleOpcode8(opcode);
        } break;

        case 0x9000: {
            handleOpcode9(opcode);
        } break;

        case 0xA000: {
            handleOpcodeA(opcode);
        } break;

        case 0xB000: {
            handleOpcodeB(opcode);
        } break;

        case 0xC000: {
            handleOpcodeC(opcode);
        } break;

        case 0xD000: {
            handleOpcodeD(opcode);
        } break;

        case 0xE000: {
            handleOpcodeE(opcode);
        } break;

        case 0xF000: {
            handleOpcodeF(opcode);
        } break;

        default: {
            std::cout << "Unknown opcode: 0x" << std::hex << opcode << std::endl;
            CPU::pc += 2;
        } break;
    }
}

void Interpreter::handleOpcode0(uint16_t opcode) {
    switch (opcode & 0x0FFF) {
        case 0x00E0: {  // 0x00E0: Clears the screen
            std::fill(&CPU::gfx.at(0, 0), &CPU::gfx(0, 0) + sizeof(CPU::gfx), 0);
            CPU::pc += 2;
        } break;

        case 0x00EE: {  // 0x00EE: Returns from subroutine
            CPU::pc = CPU::stack.at(CPU::sp);
            CPU::sp--;
        } break;

        default: {
            std::cout << "Unknown opcode: 0x" << std::hex << opcode << std::endl;
            CPU::pc += 2;
        } break;
    }
}

void Interpreter::handleOpcode1(uint16_t opcode) {
    // 0x1NNN: Jumps to address NNN.
    CPU::pc = opcode & 0x0FFF;
}

void Interpreter::handleOpcode2(uint16_t opcode) {
    // 0x2NNN: Calls subroutine at NNN.
    CPU::pc += 2;
    CPU::sp++;
    CPU::stack.at(CPU::sp) = CPU::pc;
    CPU::pc = opcode & 0x0FFF;
}

void Interpreter::handleOpcode3(uint16_t opcode) {
    // 0x3XNN: Skips the next instruction if registers.at(X) equals NN.
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t NN = opcode & 0x00FF;        // last two hexadecimal characters of opcode
    if (CPU::registers.at(X) == NN) {
        CPU::pc += 4;
    } else {
        CPU::pc += 2;
    }
}

void Interpreter::handleOpcode4(uint16_t opcode) {
    // 0x4XNN: Skips the next instruction if registers.at(X) doesn't equal NN.
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t NN = opcode & 0x00FF;        // last two hexadecimal characters of opcode
    if (CPU::registers.at(X) != NN) {
        CPU::pc += 4;
    } else {
        CPU::pc += 2;
    }
}

void Interpreter::handleOpcode5(uint16_t opcode) {
    // 0x5XY0: Skips the next instruction if registers.at(X) equals registers.at(Y).
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
    if (CPU::registers.at(X) == CPU::registers.at(Y)) {
        CPU::pc += 4;
    } else {
        CPU::pc += 2;
    }
}

void Interpreter::handleOpcode6(uint16_t opcode) {
    // 0x6XNN: Sets registers.at(X) to NN.
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t NN = opcode & 0x00FF;        // last two hexadecimal characters of opcode
    CPU::registers.at(X) = NN;
    CPU::pc += 2;
}

void Interpreter::handleOpcode7(uint16_t opcode) {
    // 0x7XNN: Adds NN to registers.at(X).
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t NN = opcode & 0x00FF;        // last two hexadecimal characters of opcode
    CPU::registers.at(X) += NN;
    CPU::pc += 2;
}

void Interpreter::handleOpcode8(uint16_t opcode) {
    switch (opcode & 0x000F) {
        case 0x0000: {                           // 0x8XY0: Sets registers.at(X) to the value of registers.at(Y).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
            CPU::registers.at(X) = CPU::registers.at(Y);
            CPU::pc += 2;
        } break;

        case 0x0001: {                           // 0x8XY1: Sets registers.at(X) to (registers.at(X) | registers.at(Y)).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
            CPU::registers.at(X) = (CPU::registers.at(X) | CPU::registers.at(Y));
            CPU::pc += 2;
        } break;

        case 0x0002: {                           // 0x8XY2: Sets registers.at(X) to (registers.at(X) & registers.at(Y)).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
            CPU::registers.at(X) = (CPU::registers.at(X) & CPU::registers.at(Y));
            CPU::pc += 2;
        } break;

        case 0x0003: {                           // 0x8XY3: Sets registers.at(X) to (registers.at(X) ^ registers.at(Y)).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
            CPU::registers.at(X) = (CPU::registers.at(X) ^ CPU::registers.at(Y));
            CPU::pc += 2;
        } break;

        case 0x0004: {  // 0x8XY4: Adds registers.at(Y) to registers.at(X). registers.at(F) is set to 1
            // when there's a carry, and to 0 when there isn't.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode

            // Max value for any element in V is 0xFF
            // Carry occurs when (registers.at(X) + registers.at(Y)) > 0xFF but this causes a byte
            // overflow so the condition is: registers.at(X) > (0xFF - registers.at(Y))
            if (CPU::registers.at(Y) > (0xFF - CPU::registers.at(X))) {
                CPU::registers.at(0xF) = 1;  // carry
            } else {
                CPU::registers.at(0xF) = 0;  // no carry
            }
            CPU::registers.at(X) += CPU::registers.at(Y);
            CPU::pc += 2;
        } break;

        case 0x0005: {  // 0x8XY5: registers.at(Y) is subtracted from registers.at(X). registers.at(F)
            // is set to 0 when there's a borrow, and 1 when there isn't.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode

            // Borrow occurs when registers.at(X) < registers.at(Y)
            if (CPU::registers.at(X) > CPU::registers.at(Y)) {
                CPU::registers.at(0xF) = 1;  // no borrow
            } else {
                CPU::registers.at(0xF) = 0;  // borrow
            }
            CPU::registers.at(X) -= CPU::registers.at(Y);
            CPU::pc += 2;
        } break;

        case 0x0006: {  // 0x8XY6: Shifts registers.at(X) right by one. registers.at(F) is set
            // to the value of the least significant bit of registers.at(X) before the shift.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::registers.at(0xF) = CPU::registers.at(X) & 1;
            CPU::registers.at(X) = (CPU::registers.at(X) >> 1);
            CPU::pc += 2;
        } break;

        case 0x0007: {  // 0x8XY7: Sets registers.at(X) to registers.at(Y) minus registers.at(X).
            // registers.at(F) is set to 0 when there's a borrow, and 1 when there isn't.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode

            // Borrow occurs when registers.at(Y) < registers.at(X)
            if (CPU::registers.at(Y) > CPU::registers.at(X)) {
                CPU::registers.at(0xF) = 1;  // no borrow
            } else {
                CPU::registers.at(0xF) = 0;  // borrow
            }

            CPU::registers.at(X) = CPU::registers.at(Y) - CPU::registers.at(X);
            CPU::pc += 2;
        } break;

        case 0x000E: {  // 0x8XYE: Shifts registers.at(X) left by one. registers.at(F) is set to
            // the value of the most significant bit of registers.at(X) before the shift.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::registers.at(0xF) = (CPU::registers.at(X) & 0b10000000) >> 7;
            CPU::registers.at(X) = (CPU::registers.at(X) << 1);
            CPU::pc += 2;
        } break;

        default: {
            std::cout << "Unknown opcode: 0x" << std::hex << opcode << std::endl;
            CPU::pc += 2;
        } break;
    }
}

void Interpreter::handleOpcode9(uint16_t opcode) {
    // 0x9XY0: Skips the next instruction if registers.at(X) doesn't equal registers.at(Y).
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
    if (CPU::registers.at(X) != CPU::registers.at(Y)) {
        CPU::pc += 4;
    } else {
        CPU::pc += 2;
    }
}

void Interpreter::handleOpcodeA(uint16_t opcode) {
    // 0xANNN: Sets I to the address NNN
    CPU::I = opcode & 0x0FFF;
    CPU::pc += 2;
}

void Interpreter::handleOpcodeB(uint16_t opcode) {
    // 0xBNNN: Jumps to the address NNN plus registers.at(0).
    CPU::pc = (opcode & 0x0FFF) + CPU::registers.at(0);
}

void Interpreter::handleOpcodeC(uint16_t opcode) {
    // 0xCXNN: Sets registers.at(X) to a (random number from 0 to 255 & NN).
    uint8_t randomNumber = dist(eng);
    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t NN = opcode & 0x00FF;

    CPU::registers.at(X) = randomNumber & NN;
    CPU::pc += 2;
}

void Interpreter::handleOpcodeD(uint16_t opcode) {
    // 0xDXYN: Draws a sprite at coordinate (registers.at(X), registers.at(Y)) that has a
    // width of 8 pixels and a height of N pixels. Each row of 8 pixels is read as bit-coded
    // starting from memory location I; I value doesn’t change after the execution of this instruction.
    // registers.at(F) is set to 1 if any screen pixels are erased, and to 0 if that doesn’t happen.
    // If the sprite is positioned so part of it is outside the coordinates of the display, it wraps
    // around to the opposite side of the screen.

    uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
    uint8_t Y = (opcode & 0x00F0) >> 4;  // third hexadecimal character of opcode
    uint8_t N = opcode & 0x000F;         // fourth hexadecimal character of opcode
    uint8_t xpos = CPU::registers.at(X);
    uint8_t ypos = CPU::registers.at(Y);
    uint8_t width = 8;
    uint8_t height = N;

    CPU::registers.at(0xF) = 0;  // reset registers.at(F) register

    for (uint8_t currentPixelY = ypos; currentPixelY < ypos + height && currentPixelY < CPU::gfx.size_2d();
         currentPixelY++) {
        int row = currentPixelY - ypos;
        uint8_t rowOf8Pixels = CPU::memory.at(CPU::I + row);  // 1 element in memory is 8 bits wide which means it is 8
        // pixels (taken from the preloaded fontset)
        for (uint8_t currentPixelX = xpos; currentPixelX < xpos + width && currentPixelX < CPU::gfx.size_1d();
             currentPixelX++) {
            int col = currentPixelX - xpos;
            if ((rowOf8Pixels & (0x80 >> col)) != 0) {  // check if the current pixel in the fontset is 1 (we're
                // not using == 1 because the 1 can be anywhere in the 8-bit binary and it could equal 1, 2, 4,
                // 8, etc)
                if (CPU::gfx.at(currentPixelX, currentPixelY) == 0) {
                    CPU::gfx.at(currentPixelX, currentPixelY) = 1;
                } else {
                    CPU::gfx.at(currentPixelX, currentPixelY) = 0;
                    CPU::registers.at(0xF) = 1;
                }
            }
        }
    }

    CPU::drawFlag = true;
    CPU::pc += 2;
}

void Interpreter::handleOpcodeE(uint16_t opcode) {
    switch (opcode & 0x00FF) {
        case 0x009E: {  // 0xEX9E: Skips the next instruction if the key stored in registers.at(X) is pressed
            uint8_t X = (opcode & 0x0F00) >> 8;             // second hexadecimal character of opcode
            if (CPU::keys.at(CPU::registers.at(X)) != 0) {  // if key is pressed
                CPU::pc += 4;
            } else {
                CPU::pc += 2;
            }
        } break;

        case 0x00A1: {  // 0xEXA1: Skips the next instruction if the key stored in registers.at(X) isn't pressed
            uint8_t X = (opcode & 0x0F00) >> 8;             // second hexadecimal character of opcode
            if (CPU::keys.at(CPU::registers.at(X)) == 0) {  // if key isn't pressed
                CPU::pc += 4;
            } else {
                CPU::pc += 2;
            }
        } break;

        default: {
            std::cout << "Unknown opcode: 0x" << std::hex << opcode << std::endl;
            CPU::pc += 2;
        } break;
    }
}

void Interpreter::handleOpcodeF(uint16_t opcode) {
    switch (opcode & 0x00FF) {
        case 0x0007: {                           // 0xFX07: Sets registers.at(X) to the value of the delay timer.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::registers.at(X) = CPU::delay_timer;
            CPU::pc += 2;
        } break;

        case 0x000A: {  // 0xFX0A: A key press is awaited. Then, the hexadecimal associated to the key is stored
                        // in registers.at(X).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            while (true) {
                if (sfmlWindow.takeUserInput()) {  // loop forever until we get a key press
                    break;
                }
            }
            CPU::registers.at(X) = sfmlWindow.lastKeyChanged;
            CPU::pc += 2;
        } break;

        case 0x0015: {                           // 0xFX15: Sets the delay timer to registers.at(X).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::delay_timer = CPU::registers.at(X);
            CPU::pc += 2;
        } break;

        case 0x0018: {                           // 0xFX18: Sets the sound timer to registers.at(X).
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::sound_timer = CPU::registers.at(X);
            CPU::pc += 2;
        } break;

        case 0x001E: {  // 0xFX1E: Adds registers.at(X) to I. registers.at(F) is set to 1 when
            // there is a carry and 0 when there isn't.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode

            // Max value of I is 0xFFF
            // Carry occurs when (I + registers.at(X)) > 0xFFF
            if ((CPU::I + CPU::registers.at(X)) > 0xFFF) {
                CPU::registers.at(0xF) = 1;  // carry
            } else {
                CPU::registers.at(0xF) = 0;  // no carry
            }

            CPU::I += CPU::registers.at(X);
            CPU::pc += 2;
        } break;

        case 0x0029: {  // 0xFX29: Sets I to the memory location of the sprite
            // associated to the hexadecimal in registers.at(X). Characters 0-F (in hexadecimal) are represented
            // by a 4x5 font. The memory location of any hexadecimal sprite is 5*hexadecimal so
            // memory.at(5*hexadecimal) is the start of the hexadecimal sprite
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::I = 5 * CPU::registers.at(X);
            CPU::pc += 2;
        } break;

        case 0x0033: {  // 0xFX33: Stores the binary-coded decimal representation of registers.at(X), with the
            // most significant of three digits at the address in I, the middle digit at I plus 1, and the
            // least significant digit at I plus 2. (In other words, take the decimal representation of
            // registers.at(X), place the hundreds digit in memory at location in I, the tens digit at location
            // I+1, and the ones digit at location I+2.)
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode
            CPU::memory.at(CPU::I) = CPU::registers.at(X) / 100;
            CPU::memory.at(CPU::I + 1) = (CPU::registers.at(X) / 10) % 10;
            CPU::memory.at(CPU::I + 2) = (CPU::registers.at(X) % 100) % 10;
            CPU::pc += 2;
        } break;

        case 0x0055: {  // 0xFX55: Stores registers.at(0) to registers.at(X) in memory starting
            // at address I. I is unchanged.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode

            for (uint8_t i = 0; i <= X; i++) {
                CPU::memory.at(CPU::I + i) = CPU::registers.at(i);  // each element in memory and V are both 8 bits long
            }

            CPU::pc += 2;
        } break;

        case 0x0065: {  // 0xFX65: Fills registers.at(0) to registers.at(X) with values from
            // memory starting at address I. I is unchanged.
            uint8_t X = (opcode & 0x0F00) >> 8;  // second hexadecimal character of opcode

            for (uint8_t i = 0; i <= X; i++) {
                CPU::registers.at(i) = CPU::memory.at(CPU::I + i);  // each element in memory and V are both 8 bits long
            }

            CPU::pc += 2;
        } break;

        default: {
            std::cout << "Unknown opcode: 0x" << std::hex << opcode << std::endl;
            CPU::pc += 2;
        } break;
    }
}
