#pragma once
#include <SFML/Audio.hpp>
#include <array>
#include <array_2d.hpp>
#include <random>
#include "../CPU.h"
#include "../SFMLWindow.h"

class Interpreter {
   public:
    Interpreter(SFMLWindow& sfmlWindow);

    SFMLWindow& sfmlWindow;
    std::default_random_engine eng{std::random_device{}()};
    std::uniform_int_distribution<> dist{0, 255};

    void emulateNextOpcode();
    void emulateOpcode(uint16_t opcode);
    void handleOpcode0(uint16_t opcode);
    void handleOpcode1(uint16_t opcode);
    void handleOpcode2(uint16_t opcode);
    void handleOpcode3(uint16_t opcode);
    void handleOpcode4(uint16_t opcode);
    void handleOpcode5(uint16_t opcode);
    void handleOpcode6(uint16_t opcode);
    void handleOpcode7(uint16_t opcode);
    void handleOpcode8(uint16_t opcode);
    void handleOpcode9(uint16_t opcode);
    void handleOpcodeA(uint16_t opcode);
    void handleOpcodeB(uint16_t opcode);
    void handleOpcodeC(uint16_t opcode);
    void handleOpcodeD(uint16_t opcode);
    void handleOpcodeE(uint16_t opcode);
    void handleOpcodeF(uint16_t opcode);
};
