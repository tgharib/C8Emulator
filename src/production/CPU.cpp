#include "CPU.h"

template <class T, size_t N>
std::array<T, N> init_memory() {
    std::array<T, N> ret;
    ret.fill(0);
    for (int i = 0; i < 80; ++i) {  // Load fontset
        ret.at(i) = CPU::fontset.at(i);
    }
    return ret;
}

bool CPU::drawFlag = false;
fsma::array_2d<uint8_t, 64, 32> CPU::gfx = {{}};
std::array<uint8_t, 16> CPU::keys = {{}};
std::array<uint8_t, 4096> CPU::memory = init_memory<uint8_t, 4096>();
std::array<uint8_t, 16> CPU::registers = {{}};
uint16_t CPU::I = 0;
uint16_t CPU::pc = 0x200;
uint8_t CPU::delay_timer = 0;
uint8_t CPU::sound_timer = 0;
std::array<uint16_t, 16> CPU::stack = {{}};
uint8_t CPU::sp = 0;
const std::array<uint8_t, 80> CPU::fontset = {{0xF0, 0x90, 0x90, 0x90, 0xF0,    // 0
                                               0x20, 0x60, 0x20, 0x20, 0x70,    // 1
                                               0xF0, 0x10, 0xF0, 0x80, 0xF0,    // 2
                                               0xF0, 0x10, 0xF0, 0x10, 0xF0,    // 3
                                               0x90, 0x90, 0xF0, 0x10, 0x10,    // 4
                                               0xF0, 0x80, 0xF0, 0x10, 0xF0,    // 5
                                               0xF0, 0x80, 0xF0, 0x90, 0xF0,    // 6
                                               0xF0, 0x10, 0x20, 0x40, 0x40,    // 7
                                               0xF0, 0x90, 0xF0, 0x90, 0xF0,    // 8
                                               0xF0, 0x90, 0xF0, 0x10, 0xF0,    // 9
                                               0xF0, 0x90, 0xF0, 0x90, 0x90,    // A
                                               0xE0, 0x90, 0xE0, 0x90, 0xE0,    // B
                                               0xF0, 0x80, 0x80, 0x80, 0xF0,    // C
                                               0xE0, 0x90, 0x90, 0x90, 0xE0,    // D
                                               0xF0, 0x80, 0xF0, 0x80, 0xF0,    // E
                                               0xF0, 0x80, 0xF0, 0x80, 0x80}};  // F
