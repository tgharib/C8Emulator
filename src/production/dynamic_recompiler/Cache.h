#pragma once
#define CACHE_SIZE 4096  // CHIP-8 memory was 4096 bytes so a cache region cannot be bigger than that

#include <array>
#include <cstdint>
#include "Translator.h"

// This exists because std::array does not have a constructor to initialize all elements
template <class T, size_t N>
std::array<T, N> make_array(const T& v) {
    std::array<T, N> ret;
    ret.fill(v);
    return ret;
}

class Cache {
   public:
    Cache();

    uint16_t c8StartAddress = 0;  // The start C8 pc for this cache (code inclusive).
    uint16_t c8EndAddress = 0;    // The end C8 pc for this cache (code inclusive).
    std::array<uint8_t, CACHE_SIZE> x86memory = make_array<uint8_t, CACHE_SIZE>(0x90);

    // uint8_t* x86_mem_address;        // Used to store the base address of where the cache is stored in memory.
    // uint32_t x86_pc;                 // Used to tell how much code has been emitted to the cache (code size).
    // uint8_t stop_write_flag; // Used to signify that no more code should be emitted to this cache (usually because it
    // ends in a jump).
    // uint8_t invalid_flag;

    Translator translator{};

   private:
    asmjit::StaticRuntime runtime;
    asmjit::X86Assembler assembler;
    asmjit::X86Compiler compiler;
};
