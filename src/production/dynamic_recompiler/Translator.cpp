#include "Translator.h"
#include "InterruptGlobals.h"

void Translator::emitUseInterpreterInterrupt(asmjit::X86Assembler& assembler) {
    // TODO: Is rax a volatile register?
    // Step 1. Save current address into InterruptGlobals::x86CacheReturnAddress: lea rax, [rip]
    // mov [&InterruptGlobals::x86CacheReturnAddress], rax
    assembler.lea(asmjit::x86::rax, asmjit::x86::ptr(asmjit::x86::rip));
    assembler.mov(asmjit::x86::word_ptr_abs(asmjit::Ptr(&InterruptGlobals::x86CacheReturnAddress)), asmjit::x86::rax);
    // Step 2. Set InterruptGlobals::interruptStatusCode=1: mov [&InterruptGlobals::interruptStatusCode], 1
    assembler.mov(asmjit::x86::byte_ptr_abs(asmjit::Ptr(&InterruptGlobals::interruptStatusCode)), 1);
    // Step 3. Return to the emulator loop by doing a JMP
    assembler.jmp(InterruptGlobals::x86EmulatorReturnAddress);
}

void Translator::emitOutOfCodeInterrupt(asmjit::X86Assembler& assembler) {
    // TODO: 64-bit absolute addressing is limited to 32 bits so see if the below causes any errors

    // Step 1. Set InterruptGlobals::interruptStatusCode=2: mov [&InterruptGlobals::interruptStatusCode], 2
    assembler.mov(asmjit::x86::byte_ptr_abs(asmjit::Ptr(&InterruptGlobals::interruptStatusCode)), 2);
    // Step 2. Return to the emulator loop by doing a JMP
    assembler.jmp(InterruptGlobals::x86EmulatorReturnAddress);
}
