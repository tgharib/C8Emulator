#pragma once
#include <asmjit/asmjit.h>

class Translator {
   public:
    static void emitUseInterpreterInterrupt(asmjit::X86Assembler& assembler);
    static void emitOutOfCodeInterrupt(asmjit::X86Assembler& assembler);
};
