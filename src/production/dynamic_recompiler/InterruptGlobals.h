#pragma once
#include <stdint.h>

class InterruptGlobals {
   public:
    static uint8_t interruptStatusCode;        // 0 is PREPARE_FOR_JUMP; 1 is USE_INTERPRETER; 2 is OUT_OF_CODE
    static uint64_t x86EmulatorReturnAddress;  // used to go back to emulation loop
    static uint64_t x86CacheReturnAddress;     // if the interrupt is to use the interpreter, we'll need to resume back
                                               // to where we were

    // uint64_t is used instead of void* because pointers do not have any size guarantees in C++ and a size guarantee is
    // needed for asmjit
};
