#include "Cache.h"
#include <iostream>

Cache::Cache() : runtime(&(x86memory.at(0)), CACHE_SIZE), assembler(&runtime), compiler(&assembler) {
    Translator::emitUseInterpreterInterrupt(assembler);
    // TODO: only call below method until after everything else is finished
    Translator::emitOutOfCodeInterrupt(assembler);
    std::cout << assembler.make() << std::endl;
}
