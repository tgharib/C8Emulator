#include "InterruptGlobals.h"

uint8_t InterruptGlobals::interruptStatusCode = 0;
uint64_t InterruptGlobals::x86EmulatorReturnAddress = 0;
uint64_t InterruptGlobals::x86CacheReturnAddress = 0;
