#pragma once

#include <memory>
#include "Cache.h"

class CachesManager {
   public:
    CachesManager();

   private:
    std::unique_ptr<std::array<Cache, 4096>> caches{new std::array<Cache, 4096>};
    // since there are only 4096 bytes in memory, it is impossible to have
    // more than 4096 cache regions
    // cache regions are allocated on heap to prevent stack overflow

    size_t currentCacheIndex = 0;
};
